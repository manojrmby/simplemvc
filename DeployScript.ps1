$appName = "SimpleMVC"
$physicalPath = "D:\Hosting\AutoDeploy\SimpleMVC"
$siteName = "Default Web Site"
[System.Reflection.Assembly]::LoadWithPartialName(“Microsoft.Web.Administration”)
$sm = New-Object Microsoft.Web.Administration.ServerManager
$appPool = $sm.ApplicationPools[$appName]
if ($appPool -eq $null) {
    $appPool = $sm.ApplicationPools.Add($appName)
}
$site = $sm.Sites[$siteName]
$app = $site.Applications["/$appName"]
if ($app -eq $null) {
    $app = $site.Applications.Add("/$appName", $physicalPath)
}
$app.ApplicationPoolName = $appName
$sm.CommitChanges()